FROM ubuntu:latest
MAINTAINER Joseph Wen <wenjoseph88@gmail.com>

RUN \
  apt-get update

# Set up SSH with SSH forwarding
RUN \
  apt-get install -y openssh-server &&  \
  mkdir /var/run/sshd && \
  echo "AllowAgentForwarding yes" >> /etc/ssh/sshd_config

# Install packages
RUN \
  apt-get install -y openssh-client vim git build-essential make man cmake curl && \
  apt-get install -y ipython bash-completion && \
  apt-get install -y byobu zsh

RUN \
  apt-get install -y apt-transport-https ca-certificates && \ 
  apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D && \
  echo "deb https://apt.dockerproject.org/repo ubuntu-xenial main" > /etc/apt/sources.list.d/docker.list && \
  apt-get update && \
  apt-get install -y docker-engine

# Install docker-compose
RUN \
  curl -L https://github.com/docker/compose/releases/download/1.7.1/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose && \
  curl -L https://raw.githubusercontent.com/docker/compose/$(docker-compose version --short)/contrib/completion/bash/docker-compose > /etc/bash_completion.d/docker-compose && \
  chmod +x /usr/local/bin/docker-compose

# Install TypeScript
RUN \
  curl -sL https://deb.nodesource.com/setup_6.x | bash - && \
  apt-get install -y nodejs && \
  npm install -g typescript

# Install vim with python 2 support
RUN apt-get install -y python-dev python3-dev vim-nox-py2

# Download configs & copy to the correct location
RUN mkdir /root/.vim

COPY config/git/.gitconfig /root
COPY config/vim/.vimrc /root
COPY config/vim/.ycm_extra_conf.py /root/.vim/

# Vim config
RUN \
  git clone https://github.com/gmarik/Vundle.vim.git /root/.vim/bundle/Vundle.vim && \
  vim +PluginInstall +qall > /dev/null 4>&1

RUN \
  cd /root/.vim/bundle/YouCompleteMe && ./install.sh --clang-completer --tern-completer && \
  cd /root/.vim/bundle/vimproc.vim && make

# Go
RUN \
  apt-get install -y golang

# Generate UTF-8 locale
RUN \
  locale-gen en_US en_US.UTF-8 && dpkg-reconfigure locales

# Get public key for login from git
RUN \
  mkdir /root/.ssh && \
  touch /root/.ssh/authorized_keys && \
  git clone https://wenjoseph@bitbucket.org/wenjoseph/pubkey.git /root/pubkey/ && \
  cat /root/pubkey/id_rsa.pub >> /root/.ssh/authorized_keys

# SSH
EXPOSE 22

VOLUME ["/root/work"]

CMD /usr/sbin/sshd -D
