# Build Docker image:

    $ docker build -t env:latest devenv

# Start Docker container with SSH port on 10022:

    $ cd devenv
    $ docker-compose up

# Add ssh alias name
add the following to `~/.ssh/config`

    Host docker-env
    User root
    HostName 127.0.0.1
    Port 10022
    IdentityFile ~/.ssh/id_rsa.docker

then use `ssh docker-dev` to login.
