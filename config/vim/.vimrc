" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'Shougo/unite.vim'
Plugin 'Shougo/neomru.vim'
Plugin 'Shougo/vimproc.vim'
Plugin 'derekwyatt/vim-fswitch'
Plugin 'tpope/vim-abolish'
Plugin 'Valloric/YouCompleteMe'
Plugin 'leafgarland/typescript-vim'

call vundle#end()

" General settings.
filetype plugin indent on
syntax on
set ruler
set laststatus=2
set hlsearch
let mapleader='\'

" YCM settings.
let g:ycm_confirm_extra_conf = 0
let g:ycm_global_ycm_extra_conf = '/root/.vim/.ycm_extra_conf.py'

autocmd BufRead,BufNewFile *.ts setlocal filetype=typescript
